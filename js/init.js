
function scrollToAnchor(aid){
    var aTag = $("#"+aid+"");
    $('html,body').animate({scrollTop: aTag.offset().top},'slow');
}

(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
		$('#register-button').click(function(e) {
			e.preventDefault();

			scrollToAnchor('register');
		})
		$('#location-button').click(function(e) {
			e.preventDefault();

			scrollToAnchor('location');
		})



  }); // end of document ready
})(jQuery); // end of jQuery name space

